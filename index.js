"use strict";

/* 1 */
let inputNum = 2;
const getCube = Math.pow(inputNum, 3);
console.log(`The cube of ${inputNum} is ${getCube}`);

/* 2 */
const address = ["258", "Washington Ave", "NW", "California", "90011"];
const [addressNumber, city, stateCd, state, zipCode] = address;
console.log(`I live at ${addressNumber} ${city} ${stateCd}, ${state} ${zipCode}`);

/* 3 */
const animal = {
    name: "Lolong",
    species: "Saltwater Crocodile",
    weight: 1075,
    measurement: {
        feet: 20,
        inch: 3
    }
}

const {name, species, weight, measurement} = animal;
const {feet, inch} = measurement;
console.log(`${name} was a ${species.toLowerCase()}. He weighed at ${weight} kgs with a measurement of ${feet} ft ${inch} in.`);

/* 4 */
const arrayNum = [1, 2, 3, 4, 5];
arrayNum.forEach((num) => console.log(num));

const reduceNumber = arrayNum.reduce((x, y) => x+y);
console.log(reduceNumber);

/* 5 */
class Dog {
    constructor (name, age, breed) {
        this.name = name;
        this.age = age;
        this.breed = breed;
    }
}

const dog = new Dog("Bond", "5", "Great Pyrenees");
console.log(dog);